package ru.ibs.hadoop.dto;

import lombok.Data;

@Data
public class ResponseDTO {
    private String message;

    public ResponseDTO(String message) {
        this.message = message;
    }
}
