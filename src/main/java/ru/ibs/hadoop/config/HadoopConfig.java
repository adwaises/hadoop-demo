package ru.ibs.hadoop.config;

import org.apache.hadoop.fs.FileSystem;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
class HadoopConfig {
    @Bean
    public FileSystem getConnection() throws IOException {
        System.setProperty("HADOOP_USER_NAME", "osboxes");

        org.apache.hadoop.conf.Configuration configuration = new org.apache.hadoop.conf.Configuration();
        configuration.set("fs.defaultFS", "hdfs://192.168.40.131:9000");
        return FileSystem.get(configuration);
    }
}