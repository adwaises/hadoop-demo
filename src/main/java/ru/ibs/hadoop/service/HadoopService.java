package ru.ibs.hadoop.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface HadoopService {
    void createDir(String path) throws IOException;

    void createFile(MultipartFile file, String path) throws IOException;

    void appendFile(String filepath, String message) throws IOException;

    byte[] readFile(String filepath) throws IOException;

    void deleteFile(String filepath) throws IOException;
}
