package ru.ibs.hadoop.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

@Service
@RequiredArgsConstructor
public class HadoopServiceImpl implements HadoopService {
    private final FileSystem fileSystem;

    @Override
    public void createDir(String path) throws IOException {
        fileSystem.mkdirs(new Path(path));
    }

    @Override
    public void createFile(MultipartFile file, String path) throws IOException {
        FSDataOutputStream fsDataOutputStream =
                fileSystem.create(new Path(path + "/" + file.getOriginalFilename()), true);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fsDataOutputStream);
        bufferedOutputStream.write(file.getBytes());
        bufferedOutputStream.close();
    }

    @Override
    public void appendFile(String filepath, String message) throws IOException {
        Path hdfsWritePath = new Path(filepath);
        FSDataOutputStream fsDataOutputStream = fileSystem.append(hdfsWritePath);
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fsDataOutputStream, StandardCharsets.UTF_8));
        bufferedWriter.newLine();
        bufferedWriter.write(message);
        bufferedWriter.close();
    }

    @Override
    public byte[] readFile(String filepath) throws IOException {
        FSDataInputStream in = fileSystem.open(new Path(filepath));
        byte[] bytes = IOUtils.toByteArray(in);
        in.close();
        return bytes;
    }

    @Override
    public void deleteFile(String filepath) throws IOException {
        fileSystem.delete(new Path(filepath), true);
    }
}
