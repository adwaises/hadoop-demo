package ru.ibs.hadoop.controller;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.ibs.hadoop.dto.ResponseDTO;
import ru.ibs.hadoop.service.HadoopService;

import java.io.IOException;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "api/v1/", produces = "application/json")
public class HadoopController {
    private final HadoopService hadoopService;

    @PostMapping("/dir/create")
    @ApiOperation("Создание директории")
    public ResponseDTO createDir(String path) throws IOException {
        hadoopService.createDir(path);
        return new ResponseDTO("OK");
    }

    @PostMapping("/file/append")
    @ApiOperation("Дозапись файла")
    public ResponseDTO appendFile(String filepath, String message) throws IOException {
        hadoopService.appendFile(filepath, message);
        return new ResponseDTO("OK");
    }

    @PostMapping("/file/create")
    @ApiOperation("Создание файла")
    public ResponseDTO createFile(MultipartFile file, String path) throws IOException {
        hadoopService.createFile(file, path);
        return new ResponseDTO("OK");
    }

    @GetMapping("/file/read")
    @ApiOperation("Чтение файла")
    public ResponseEntity<byte[]> readFile(String filepath) throws IOException {
        byte[] bytes = hadoopService.readFile(filepath);
        String filename = filepath.substring(filepath.lastIndexOf('/') + 1);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename);
        return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
    }

    @DeleteMapping("/delete")
    @ApiOperation("Удаление файла или директории")
    public ResponseDTO deleteFile(String filepath) throws IOException {
        hadoopService.deleteFile(filepath);
        return new ResponseDTO("OK");
    }


}
